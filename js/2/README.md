#Задача
Необходимо найти и устранить утечку памяти, объяснить природу утечки.
```
var theItem = null;
var replaceItem = function () {
    var priorItem = theItem;
    var writeToLog = function () {
        if (priorItem) {
            console.log("hi");
        }
    };
    theItem = {
        longStr: new Array(1000000).join("*"),
        someMethod: function () {
            console.log(someMessage);
        }
    };
};
setInterval(replaceItem, 1000);
```

#Решения app.js

Проблема в замыкании priorItem внутри writeToLog и someMethod