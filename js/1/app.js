
function Hides(elem, headerSelector){
    this.visible = true
    this.elem = elem
    this.header = elem.querySelector(headerSelector)
    this.displayInitial = this.elem.style.display

    this.header.addEventListener('click', () => {
        if(this.visible){
            this.hide()
        }else{
            this.show()
        }
    })
}

Hides.prototype.hide = function(){
    this.elem.style.display = 'none'
    this.visible = false
}

Hides.prototype.show = function(){
    this.elem.style.display = this.displayInitial
    this.visible = true
}


function HidesSmooth(){
    Hides.apply(this, arguments)
    this.overflowInitial = this.elem.style.overflow
    this.transitionInitial = this.elem.style.transition

    this.hide = () => {
        this.visible = false
        this.elem.style.overflow   = 'hidden'
        this.elem.style.transition = '.3s'
        this.elem.style.maxHeight  = 0
        setTimeout(() => {
            Hides.prototype.hide.call(this)
            this.elem.style.overflow = this.overflowInitial
            this.elem.style.transition = this.transitionInitial
        }, 300)
    }
    this.show = () => {
        this.header.style.display = this.initial
        this.elem.style.overflow   = 'hidden'
        this.elem.style.transition = '.3s'
        this.elem.style.maxHeight  = '1000000px'
        setTimeout(() => {
            Hides.prototype.show.call(this)
            this.elem.style.maxHeight = ''
            this.elem.style.overflow = this.overflowInitial
            this.elem.style.transition = this.transitionInitial
        }, 300)
    }
}
