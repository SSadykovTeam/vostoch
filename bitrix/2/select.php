<?php

use Bitrix\Main\Loader;
use Bitrix\Main\Data\Cache;

define('CITY_IBLOCK_ID', 5);
define('COURSE_IBLOCK_ID', 1);
define('RASP_COURSE_IBLOCK_ID', 6);

$CITY = 34;
$cources = [];

$cache = Cache::createInstance();
if ($cache->initCache(60 * 60 * 24, 'cources_' . $CITY . '_' . date('y_m_d'))) {
    $cources = $cache->getVars()['cources'];
} elseif ($cache->startDataCache()) {
    Loader::IncludeModule('iblock');

    $courcesActive = [];
    $cources = [];
    $res = CIBlockElement::GetList([], ['IBLOCK_ID' => RASP_COURSE_IBLOCK_ID, '>=DATE_ACTIVE_FROM' => ConvertTimeStamp(time(), 'SHORT'), '!PROPERTY_COURSE_ID' => false], false, false, ['PROPERTY_COURSE_ID']);
    while ($ob = $res->fetch()) {
        $courcesActive[] = $ob['PROPERTY_COURSE_ID_VALUE'];
    }

    if ($courcesActive) {
        $res = CIBlockElement::GetList([], ['IBLOCK_ID' => COURSE_IBLOCK_ID, 'PROPERTY_CITY' => $CITY, 'ID' => $courcesActive], false, false, ['ID']);
        while ($ob = $res->fetch()) {
            $cources[] = $ob['ID'];
        }
    }

    $cache->endDataCache(['cources' => $cources]);
}

print_r($cources);
