<?php

use Monolog\Registry;
use Bitrix\Main\Loader;
use Bitrix\Main\EventManager;

$eventManager = EventManager::getInstance();
$eventManager->addEventHandler('iblock', 'OnAfterIBlockElementUpdate', ['EventHandlers', 'onAfterIBlockElementUpdateHandler']);

class EventHandlers
{
    function onAfterIBlockElementUpdateHandler(&$arFields)
    {
        global $USER;

        $logger = Registry::getInstance('iblock1_access_log');
        $logger->info($USER->GetLogin() . ' [' . $USER->GetId() . ']');
    }
}

function sendIblock1AccessLogAgent()
{
    try {
        Loader::includeModule('form');
        $formSaveResult = CFormResult::Add(IBLOCK1_ACCESS_LOG_FORM, [
            'form_textarea_1' => file_get_contents(IBLOCK1_ACCESS_LOG_FILE),
        ]);

        if (!$formSaveResult) throw new Exception('file not saved');

        CFormCRM::onResultAdded(IBLOCK1_ACCESS_LOG_FORM, $formSaveResult);
        CFormResult::SetEvent($formSaveResult);
        CFormResult::Mail($formSaveResult);
    } catch (Exception $exc) {
        AddMessage2Log($exc->getMessage());
    }

    return 'sendIblock1AccessLogAgent();';
}
