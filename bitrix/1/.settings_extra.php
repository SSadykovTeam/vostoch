<?php
return [
    'monolog' => [
        'value' => [
            'handlers' => [
                'iblock1_access_log' => [
                    'class' => '\Monolog\Handler\StreamHandler',
                    'level' => 'DEBUG',
                    'stream' => IBLOCK1_ACCESS_LOG_FILE
                ],
            ],
            'loggers' => [
                'iblock1_access_log' => [
                    'handlers' => ['iblock1_access_log'],
                ]
            ]
        ],
        'readonly' => false
    ]
];
