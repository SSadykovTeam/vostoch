#Задача
В корзину добавляются торговые предложения, например, с помощью функции
Add2BasketByProductID. Необходимо сделать так чтобы CSaleBasket::GetList в
свойстве DETAIL_PAGE_URL возвращал ссылку на карточку товара, а не
предложения.
Какие способы вам известны?

#Решение

1. Настроить инфоблок предложений каталога чтобы у предложений был тот же url что у товара(выбрать или вписать PRODUCT_URL)
2. Передать третий параметр $rewriteFields = ['DETAIL_PAGE_URL'] [Add2BasketByProductID](https://bxapi.ru/src/?module_id=catalog&name=Add2BasketByProductID)